﻿using System;

namespace OriginDbCopy.Import
{
    class MissingSFTPFileException : ApplicationException
    {
        public MissingSFTPFileException(Table table, string extension) : base(GetMessage(table, extension))
        {
        }

        public MissingSFTPFileException(string filename) : base(string.Format("Cound not find SFTP file {0}", filename))
        {
        }

        private static string GetMessage(Table table, string extension) =>
            string.Format("Could not find SFTP file {0}{1}", table.Name, extension);
    }
}
