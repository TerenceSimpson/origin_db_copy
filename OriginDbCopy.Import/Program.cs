﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using SimpleLogging;

namespace OriginDbCopy.Import
{
    class Program
    {
        static void Main(string[] args)
        {
            if (!Directory.Exists(settings.LogPath))
            {
                Directory.CreateDirectory(settings.LogPath);
            }
            using (StaticLogger.Instance = new Logger(settings.LogLevel, new StreamWriter(Path.Combine(settings.LogPath, DateTime.Now.ToString("yyyyMMdd") + ".txt"), false)))
            {
                using (var log = StaticLogger.PushContext(nameof(Main)))
                {
                    try
                    {
                        var p = new Program();
                        p.Run();
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex);
                    }
                }
            }
        }

        static readonly Settings settings = new Settings(ConfigurationManager.AppSettings);
        static readonly string[] LocalDirectories = new[]
        {
            settings.SchemaPath, settings.FormatPath, settings.DataPath, settings.ZipPath
        };

        Program()
        {
        }

        void Run()
        {
            using (var log = StaticLogger.PushContext(nameof(Run)))
            {
                log.Info("Run Started {0}", DateTime.Now);
                PreProcess();
                Process();
                PostProcess();
                log.Info("Run Finished {0}", DateTime.Now);
            }
        }

        void PreProcess()
        {
            using (var log = StaticLogger.PushContext(nameof(PreProcess)))
            {
                log.Info("PreProcess Started {0}", DateTime.Now);
                CreateLocalPaths();
                ClearLocalPaths();
                CheckSFTPFilesExist();
                DownloadFiles();
                ExtractFiles();

                using (var sql = new SqlConnection(new SqlConnectionStringBuilder(settings.SQLConnectionInfo.ConnectionString) { ["Database"] = "master" }.ConnectionString))
                {
                    sql.Open();
                    DropDatabase(sql);
                    CreateDatabase(sql);
                    LoadSchema(sql);
                    ExecutePreScripts(sql);
                }
                log.Info("PreProcess Finished {0}", DateTime.Now);
            }
        }

        void Process()
        {
            using (var log = StaticLogger.PushContext(nameof(Process)))
            {
                log.Info("Process Started {0}", DateTime.Now);
                foreach (var table in settings.Tables)
                {
                    LoadData(table);
                }
                log.Info("Process Finished {0}", DateTime.Now);
            }
        }

        void PostProcess()
        {
            using (var log = StaticLogger.PushContext(nameof(PostProcess)))
            {
                log.Info("PostProcess Started {0}", DateTime.Now);

                using (var sql = new SqlConnection(settings.SQLConnectionInfo.ConnectionString))
                {
                    sql.Open();
                    LoadConstraints(sql);
                    ExecutePostScripts(sql);
                    ShrinkLogFile(sql);
                }

                ClearLocalPaths();

                log.Info("PostProcess Finished {0}", DateTime.Now);
            }
        }

        void CreateLocalPaths()
        {
            using (var log = StaticLogger.PushContext(nameof(CreateLocalPaths)))
            {
                log.Info("Creating local paths");
                foreach (var path in LocalDirectories)
                {
                    CreateLocalPath(path);
                }
            }
        }

        void CreateLocalPath(string path)
        {
            using (var log = StaticLogger.PushContext(nameof(CreateLocalPath)))
            {
                var info = new DirectoryInfo(path);
                if (!info.Exists)
                {
                    log.Debug("Creating {0}", path);
                    info.Create();
                }
            }
        }

        void ClearLocalPaths()
        {
            using (var log = StaticLogger.PushContext(nameof(ClearLocalPaths)))
            {
                foreach (var path in LocalDirectories)
                {
                    log.Debug("Clearing path {0}", path);
                    ClearLocalPath(path);
                }
            }
        }

        void ClearLocalPath(string path)
        {
            using (var log = StaticLogger.PushContext(nameof(ClearLocalPath)))
            {
                var entries = new DirectoryInfo(path).GetFiles();
                foreach (var entry in entries)
                {
                    log.Debug("Deleting file {0}", entry.Name);
                    entry.Delete();
                }
            }
        }

        void CheckSFTPFilesExist()
        {
            /*
             * Open a SFTP connection
             * For each Path CheckSFTPFileExists(sftp, ".{sql,constraints.sql,xml,bcp}", Path)
             */
            using (var log = StaticLogger.PushContext(nameof(CheckSFTPFilesExist)))
            {
                var pathExt = new OrderedDictionary
                {
                    { settings.SFTPSchemaPath, "schema.zip" },
                    { settings.SFTPFormatPath, "format.zip" },
                    { settings.SFTPDataPath, "data.zip" }
                };

                var en = pathExt.GetEnumerator();
                while (en.MoveNext())
                {
                    var path = (string)en.Key;
                    var filename = (string)en.Value;
                    log.Debug("Checking file {0} exists", Path.Combine(path, filename));
                    if (!File.Exists(Path.Combine(path, filename)))
                    {
                        throw new MissingSFTPFileException(filename);
                    }
                }
            }
        }

        void DownloadFiles()
        {
            using (var log = StaticLogger.PushContext(nameof(DownloadFiles)))
            {
            }
        }

        void ExtractFiles()
        {
            using (var log = StaticLogger.PushContext(nameof(ExtractFiles)))
            {
                log.Info("Extracting ZIPs");
                // Extract ZIP files
                System.IO.Compression.ZipFile.ExtractToDirectory(Path.Combine(settings.SFTPSchemaPath, "schema.zip"), settings.SchemaPath);
                System.IO.Compression.ZipFile.ExtractToDirectory(Path.Combine(settings.SFTPFormatPath, "format.zip"), settings.FormatPath);
                System.IO.Compression.ZipFile.ExtractToDirectory(Path.Combine(settings.SFTPDataPath, "data.zip"), settings.DataPath);
            }
        }

        void LoadSchema(SqlConnection sql)
        {
            using (var log = StaticLogger.PushContext(nameof(LoadSchema)))
            {
                var scriptFile = Path.Combine(settings.SchemaPath, "schema.sql");
                ExecuteSqlScript(scriptFile, sql);
            }
        }

        void LoadConstraints(SqlConnection sql)
        {
            using (var log = StaticLogger.PushContext(nameof(LoadConstraints)))
            {
                var scriptFile = Path.Combine(settings.SchemaPath, "constraints.sql");
                ExecuteSqlScript(scriptFile, sql);
            }
        }

        void LoadData(Table table)
        {
            /*
             * Import <table>.bcp using <table>.xml
             */
            using (var log = StaticLogger.PushContext(nameof(LoadData)))
            {
                var dataFile = Path.Combine(settings.DataPath, table.Name + ".bcp");
                var formatFile = Path.Combine(settings.FormatPath, table.Name + ".xml");
                log.Info("Importing data from {0} into {1} using {2}", dataFile, table.Name, formatFile);
                var bcp = new BCP(settings.SQLConnectionInfo);
                bcp.ImportDataFile(table, dataFile, formatFile, settings);
            }
        }

        void CreateDatabase(SqlConnection sql)
        {
            using (var log = StaticLogger.PushContext(nameof(CreateDatabase)))
            {
                log.Info("Creating database");
                ExecuteSql($"CREATE DATABASE [{settings.SQLConnectionInfo.Database}]", sql, "master");
                log.Info("Setting database revovery model to 'Simple'");
                ExecuteSql($"ALTER DATABASE [{settings.SQLConnectionInfo.Database}] SET RECOVERY SIMPLE", sql, "master");
            }
        }

        void DropDatabase(SqlConnection sql)
        {
            using (var log = StaticLogger.PushContext(nameof(DropDatabase)))
            {
                log.Info("Dropping database");
                ExecuteSql($"IF EXISTS(SELECT 1 FROM sys.databases WHERE name = '{settings.SQLConnectionInfo.Database}') DROP DATABASE [{settings.SQLConnectionInfo.Database}]", sql, "master");
            }
        }

        void ShrinkLogFile(SqlConnection sql)
        {
            using (var log = StaticLogger.PushContext(nameof(ShrinkLogFile)))
            {
                log.Info("Shrinking Database Log file");
                ExecuteSql("DBCC SHRINKDATABASE(0)", sql);
            }
        }

        void ExecutePreScripts(SqlConnection sql)
        {
            using (var log = StaticLogger.PushContext(nameof(ExecutePreScripts)))
            {
                ExecuteDirectoryScripts(settings.PreScriptsPath, sql);
            }
        }

        void ExecutePostScripts(SqlConnection sql)
        {
            using (var log = StaticLogger.PushContext(nameof(ExecutePreScripts)))
            {
                ExecuteDirectoryScripts(settings.PostScriptsPath, sql);
            }
        }

        void ExecuteDirectoryScripts(string path, SqlConnection sql)
        {
            var scriptFiles = Directory.GetFiles(path, "*.sql", SearchOption.TopDirectoryOnly).OrderBy(scriptFile => scriptFile).ToArray();
            foreach (var scriptFile in scriptFiles)
            {
                ExecuteSqlScript(scriptFile, sql);
            }
        }

        void ExecuteSqlScript(string scriptFile, SqlConnection sql, string database = null)
        {
            StaticLogger.Info("Executing script: {0}", scriptFile);
            var sqlScript = File.ReadAllText(scriptFile);
            ExecuteSql(sqlScript, sql, database);
        }

        void ExecuteSql(string sqlScript, SqlConnection sql, string database = null)
        {
            if (string.IsNullOrEmpty(database))
            {
                database = settings.SQLConnectionInfo.Database;
            }

            sql.ChangeDatabase(database);
            using (var command = sql.CreateCommand())
            {
                command.CommandText = sqlScript;
                command.ExecuteNonQuery();
            }
        }
    }
}
