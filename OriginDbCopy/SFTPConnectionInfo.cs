﻿using System.Collections.Specialized;

namespace OriginDbCopy
{
    public class SFTPConnectionInfo
    {
        public string Server { get; }
        public int Port { get; }
        public string UserId { get; }
        public string Password { get; }

        public static int DefaultPort => 22;

        public SFTPConnectionInfo(string server, int port, string userId, string password)
        {
            Server = server;
            Port = port;
            UserId = userId;
            Password = password;
        }

        public SFTPConnectionInfo(string server, string userId, string password) : this(server, DefaultPort, userId, password)
        {
        }

        public SFTPConnectionInfo(NameValueCollection appSettings)
        {
            Server = appSettings["SFTPServer"];
            Port = string.IsNullOrEmpty(appSettings["SFTPPort"]) ? DefaultPort : int.Parse(appSettings["SFTPPort"]);
            UserId = appSettings["SFTPUserId"];
            Password = appSettings["SFTPPassword"];
        }
    }
}
