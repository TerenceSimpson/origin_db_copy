﻿using System.Collections.Generic;

namespace OriginDbCopy
{
    using SchemaTablePair = KeyValuePair<string, string>;

    public class Table
    {
        static SchemaTablePair SplitIdentifier(string identifier)
        {
            string schemaPart = null, tablePart = null;
            if (identifier.StartsWith("["))
            {
                int pos = identifier.IndexOf("].");
                if (pos > 0)
                {
                    schemaPart = identifier.Substring(1, pos - 1);
                    tablePart =  SplitIdentifier(identifier.Substring(pos + 2)).Value;
                }
                else if (identifier.EndsWith("]"))
                {
                    tablePart = identifier.Substring(1, identifier.Length - 2);
                }
                else
                {
                    tablePart = identifier;
                }
            }
            else
            {
                int pos = identifier.IndexOf(".");
                if (pos > 0)
                {
                    schemaPart = identifier.Substring(0, pos - 1);
                    tablePart = SplitIdentifier(identifier.Substring(pos + 1)).Value;
                }

                tablePart = identifier;
            }

            return new SchemaTablePair(schemaPart, tablePart);
        }
        public Table(string schema, string name)
        {
            Schema = SplitIdentifier(schema).Value;
            Name = SplitIdentifier(name).Value;
        }

        public Table(string name)
        {
            var schemaTable = SplitIdentifier(name);
            Schema = schemaTable.Key;
            Name = schemaTable.Value;
        }

        public string Schema
        {
            get;
            private set;
        }

        public string Name
        {
            get;
            private set;
        }

        public string FullName =>
            string.IsNullOrEmpty(Schema) ? string.Format("[dbo].[{0}]", Name) : string.Format("[{0}].[{1}]", Schema, Name);

        public string ObjectName(SQLConnectionInfo info) =>
            string.Format("[{0}].{1}", info.Database, FullName);
    }
}
