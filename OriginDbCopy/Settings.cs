﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using SimpleLogging;

namespace OriginDbCopy
{
    public struct Settings
    {
        public readonly SQLConnectionInfo SQLConnectionInfo;
        public readonly SFTPConnectionInfo SFTPConnectionInfo;
        public readonly IReadOnlyList<Table> Tables;

        public readonly string SchemaPath;
        public readonly string FormatPath;
        public readonly string DataPath;
        public readonly string PreScriptsPath;
        public readonly string PostScriptsPath;
        public readonly string ZipPath;
        public readonly string LogPath;
        public readonly LogLevel LogLevel;

        public readonly string SFTPSchemaPath;
        public readonly string SFTPFormatPath;
        public readonly string SFTPDataPath;

        public readonly string SMTPServer;
        public readonly string SMTPUser;
        public readonly string SMTPPassword;
        public readonly string SMTPFrom;
        public readonly string SMTPTo;
        public readonly string SMTPSubject;


        public Settings(NameValueCollection appSettings)
        {
            SQLConnectionInfo = new SQLConnectionInfo(appSettings);

            Tables = appSettings[nameof(Tables)].Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries)
                .Select(t => new Table(t))
                .ToList();

            SFTPConnectionInfo = new SFTPConnectionInfo(appSettings);

            SchemaPath = appSettings[nameof(SchemaPath)];
            FormatPath = appSettings[nameof(FormatPath)];
            DataPath = appSettings[nameof(DataPath)];
            PreScriptsPath = appSettings[nameof(PreScriptsPath)];
            PostScriptsPath = appSettings[nameof(PostScriptsPath)];
            ZipPath = appSettings[nameof(ZipPath)];
            LogPath = appSettings[nameof(LogPath)];
            if (!Enum.TryParse(appSettings[nameof(LogLevel)], out LogLevel))
            {
                LogLevel = LogLevel.None;
            }

            SFTPSchemaPath = appSettings[nameof(SFTPSchemaPath)];
            SFTPFormatPath = appSettings[nameof(SFTPFormatPath)];
            SFTPDataPath = appSettings[nameof(SFTPDataPath)];

            SMTPServer = appSettings[nameof(SMTPServer)];
            SMTPUser = appSettings[nameof(SMTPUser)];
            SMTPPassword = appSettings[nameof(SMTPPassword)];
            SMTPFrom = appSettings[nameof(SMTPFrom)];
            SMTPTo = appSettings[nameof(SMTPTo)];
            SMTPSubject = appSettings[nameof(SMTPSubject)];
        }

        public NameValueCollection AsNameValueCollection()
        {
            return new NameValueCollection
            {
                { nameof(SchemaPath), SchemaPath },
                { nameof(FormatPath), FormatPath },
                { nameof(DataPath), DataPath },
                { nameof(ZipPath), ZipPath },
                { nameof(LogPath), LogPath },
                { nameof(SFTPSchemaPath), SFTPSchemaPath },
                { nameof(SFTPFormatPath), SFTPFormatPath },
                { nameof(SFTPDataPath), SFTPDataPath },
                { nameof(LogLevel), LogLevel.ToString() },
                { "SQLDatabase", SQLConnectionInfo.Database },
                { "SQLServer", SQLConnectionInfo.Server },
                { "SQLUserId", SQLConnectionInfo.UserId },
                { "SQLPassword", SQLConnectionInfo.Password },
                { "SFTPServer", SFTPConnectionInfo.Server },
                { "SFTPUserId", SFTPConnectionInfo.UserId },
                { "SFTPPassword", SFTPConnectionInfo.Password },
                { "SFTPPort", SFTPConnectionInfo.Port.ToString() },
                { nameof(SMTPServer), SMTPServer },
                { nameof(SMTPUser), SMTPUser },
                { nameof(SMTPPassword), SMTPPassword },
                { nameof(SMTPFrom), SMTPFrom },
                { nameof(SMTPTo), SMTPTo },
                { nameof(SMTPSubject), SMTPSubject }
            };
        }
    }
}
