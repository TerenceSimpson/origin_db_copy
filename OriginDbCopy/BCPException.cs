﻿using System;

namespace OriginDbCopy
{
    public class BCPException : ApplicationException
    {
        private BCPException(string message) : base(message)
        {
        }

        public BCPException(Table table, string action, string errorMessage) : this(GetMessage(table, action, errorMessage))
        {
        }

        private static string GetMessage(Table table, string action, string errorMessage)
        {
            var actionDesc = "Import";
            if (action == "OUT")
            {
                actionDesc = "Export";
            }
            else if (action == "FORMAT NUL")
            {
                actionDesc = "Format Generation";
            }
            return string.Format("During {0} of {1}: {2}", actionDesc, table.FullName, errorMessage);
        }
    }
}
