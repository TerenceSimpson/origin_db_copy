﻿using System.Diagnostics;
using System.Text;
using SimpleLogging;

namespace OriginDbCopy
{

    public class BCP
    {
        public BCP(SQLConnectionInfo conInfo)
        {
            this.conInfo = conInfo;
        }

        private readonly SQLConnectionInfo conInfo;

        private string GetArguments(string inputArgs)
        {
            var argBuilder = new StringBuilder(inputArgs);
            argBuilder.Append(" -S \"")
                .Append(conInfo.Server)
                .Append('"');

            if (conInfo.TrustedConnection)
            {
                argBuilder.Append(" -T");
            }
            else
            {
                argBuilder.Append(" -U \"").Append(conInfo.UserId).Append("\" ")
                    .Append(" -P \"").Append(conInfo.Password).Append('"');
            }

            return argBuilder.ToString();
        }

        private void ExecuteBCP(Table dbObject, string action, string arguments, Settings settings)
        {
            using (var log = SimpleLogging.StaticLogger.PushContext(nameof(ExecuteBCP))) {
            arguments = GetArguments(arguments);
            log.Debug("Running: bcp \"{0}\" {1} {2}", dbObject.ObjectName(conInfo), action, arguments);
                using (var bcp = new Process())
                {
                    bcp.StartInfo = new ProcessStartInfo("bcp")
                    {
                        Arguments = string.Format("\"{0}\" {1} {2}", dbObject.ObjectName(conInfo), action, arguments),
                        CreateNoWindow = true,
                        UseShellExecute = false,
                        RedirectStandardError = true,
                        RedirectStandardOutput = true
                    };

                    bcp.Start();
                    var outp = bcp.StandardOutput.ReadToEnd();
                    var error = bcp.StandardError.ReadToEnd();
                    bcp.WaitForExit();

                    if (bcp.ExitCode != 0)
                    {
                        throw new BCPException(dbObject, action, string.IsNullOrEmpty(error) ? outp : error);
                    }
                }
            }
        }

        /*
         * BCP: bcp <action> <arguments> [<datafile>] [<arguments>]
         * BCP Create Format: bcp <dbObject> FORMAT NUL -f <exportPath> -c -x
         * BCP Export Table: bcp <dbObject> OUT <exoprtPath> -f <formatFile> -c
         * BCP Import Table: bcp <dbObject> IN <importFile> -f <formatFile> -c -E
         */

        public void CreateFormatFile(Table dbObject, string outputFile, Settings settings) =>
            ExecuteBCP(dbObject, "FORMAT NUL", string.Format("-f \"{0}\" -x -n", outputFile), settings);

        public void ExportDataFile(Table dbObject, string outputFile, Settings settings) =>
            ExecuteBCP(dbObject, "OUT", string.Format("\"{0}\" -n", outputFile), settings);
        public void ExportDataFile(Table dbObject, string outputFile, string formatFile, Settings settings) =>
            ExecuteBCP(dbObject, "OUT", string.Format("\"{0}\" -f \"{1}\"", outputFile, formatFile), settings);

        public void ImportDataFile(Table dbObject, string inputFile, Settings settings) =>
            ExecuteBCP(dbObject, "IN", string.Format("\"{0}\" -n -E", inputFile), settings);
        public void ImportDataFile(Table dbObject, string inputFile, string formatFile, Settings settings) =>
            ExecuteBCP(dbObject, "IN", string.Format("\"{0}\" -f \"{1}\" -E", inputFile, formatFile), settings);
    }
}
