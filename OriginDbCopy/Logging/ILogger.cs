﻿using System;
using System.IO;

namespace SimpleLogging
{
    public interface ILogger : IDisposable
    {
        TextWriter OutputStream { get; }
        LogLevel LogLevel { get; set; }
        string Context { get; set; }
        void Log(LogLevel logLevel, string message, params object[] fmtArgs);

        ILogger PushContext(string context);
        void PopContext();
    }
}
