﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SimpleLogging
{
    public class Logger : ILogger
    {
        TextWriter outputStream = Console.Out;
        bool ownOutputStream = false;

        LogLevel logLevel = LogLevel.Debug;
        Stack<string> contexts = new Stack<string>();

        public TextWriter OutputStream => outputStream;
        public LogLevel LogLevel
        {
            get => logLevel;
            set => logLevel = value;
        }

        public string Context
        {
            get => contexts.Peek() ?? string.Empty;
            set => contexts.Push(value);
        }

        public Logger() { }

        public Logger(LogLevel logLevel) => this.logLevel = logLevel;

        public Logger(TextWriter outputStream)
        {
            this.outputStream = outputStream ?? throw new ArgumentNullException(nameof(outputStream));
            ownOutputStream = true;
        }

        public Logger(LogLevel logLevel, TextWriter outputStream, bool ownOutputStream = true)
        {
            this.logLevel = logLevel;
            this.outputStream = outputStream ?? throw new ArgumentNullException(nameof(outputStream));
            this.ownOutputStream = ownOutputStream;
        }

        public void Log(LogLevel logLevel, string message, params object[] fmtArgs)
        {
            if (logLevel >= LogLevel)
            {
                var sb = new StringBuilder();
                sb.AppendFormat("[{0}]{{{1}}}", DateTime.Now, logLevel);
                var context = Context;
                if (!string.IsNullOrEmpty(context))
                {
                    sb.AppendFormat("[{0}]", context);
                }
                sb.Append(' ').AppendFormat(message, fmtArgs);

                outputStream.WriteLine(sb.ToString());
                outputStream.Flush();
                if (outputStream != Console.Out)
                {
                    Console.WriteLine(sb.ToString());
                    Console.Out.Flush();
                }
            }
        }

        public ILogger PushContext(string context) => new LogContextWrapper(this, context);

        public void PopContext() => contexts.Pop();

        public void Dispose() => Dispose(true);

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (ownOutputStream)
                {
                    ownOutputStream = false;
                    var stream = outputStream;
                    outputStream = null;
                    stream?.Dispose();
                }
            }
        }
    }
}
