﻿using System;
using System.IO;

namespace SimpleLogging
{
    public class LogContextWrapper : ILogger
    {
        readonly ILogger baseLogger;

        public TextWriter OutputStream => baseLogger.OutputStream;

        public LogLevel LogLevel
        {
            get => baseLogger.LogLevel;
            set => baseLogger.LogLevel = value;
        }

        public string Context
        {
            get => baseLogger.Context;
            set => baseLogger.Context = value;
        }

        internal LogContextWrapper(ILogger baseLogger, string context)
        {
            this.baseLogger = baseLogger ?? throw new ArgumentNullException(nameof(baseLogger));
            baseLogger.Context = context;
        }

        public void Log(LogLevel logLevel, string message, params object[] fmtArgs) => baseLogger.Log(logLevel, message, fmtArgs);

        public ILogger PushContext(string context) => baseLogger.PushContext(context);

        public void PopContext() => baseLogger.PopContext();

        public void Dispose() => Dispose(true);

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                baseLogger?.PopContext();
            }
        }
    }
}
