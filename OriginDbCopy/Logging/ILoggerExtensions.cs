﻿using System;
using System.Configuration;
using System.IO;

namespace SimpleLogging
{
    public static class ILoggerExtensions
    {
        public static void Debug(this ILogger self, string message, params object[] fmtArgs) => self.Log(LogLevel.Debug, message, fmtArgs);
        public static void Info(this ILogger self, string message, params object[] fmtArgs) => self.Log(LogLevel.Info, message, fmtArgs);
        public static void Warning(this ILogger self, string message, params object[] fmtArgs) => self.Log(LogLevel.Warning, message, fmtArgs);
        public static void Error(this ILogger self, string message, params object[] fmtArgs)
        {
            self.Log(LogLevel.Error, message, fmtArgs);
            throw new ApplicationException(string.Format(message, fmtArgs));
        }
        public static void Error(this ILogger self, Exception ex)
        {
            self.Error(ex.ToString());
            try
            {
                var mail = new System.Net.Mail.MailMessage(ConfigurationManager.AppSettings["SMTPFrom"], ConfigurationManager.AppSettings["SMTPTo"], ConfigurationManager.AppSettings["SMTPSubject"], ex.ToString())
                {
                    IsBodyHtml = false
                };
                var logFile = Path.Combine(ConfigurationManager.AppSettings["LogPath"], DateTime.Now.ToString("yyyyMMdd") + ".txt");
                mail.Attachments.Add(new System.Net.Mail.Attachment(logFile));
                using (var client = new System.Net.Mail.SmtpClient(ConfigurationManager.AppSettings["SMTPServer"]))
                {
                    client.EnableSsl = true;
                    client.UseDefaultCredentials = false;
                    client.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPUser"], ConfigurationManager.AppSettings["SMTPPassword"]);
                    client.Send(mail);
                }
            }
            catch
            {
                // Swallow errors caused by handleing errors
            }
        }
    }
}
