﻿namespace SimpleLogging
{
    public enum LogLevel
    {
        None,
        Debug, Info,
        Warning, Error
    }
}
