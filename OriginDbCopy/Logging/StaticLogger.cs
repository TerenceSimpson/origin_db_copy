﻿using System;
using System.IO;

namespace SimpleLogging
{
    public static class StaticLogger
    {
        static ILogger instance = new Logger();

        public static TextWriter OutputStream => instance.OutputStream;

        public static ILogger Instance
        {
            get => instance;
            set
            {
                instance = value ?? throw new ArgumentNullException(nameof(value));
            }
        }

        public static string Context => Instance.Context;

        public static LogLevel LogLevel
        {
            get => Instance.LogLevel;
            set => Instance.LogLevel = value;
        }

        public static void Log(LogLevel logLevel, string message, params object[] fmtArgs) => instance.Log(logLevel, message, fmtArgs);

        public static ILogger PushContext(string context) => instance.PushContext(context);

        public static void PopContext() => instance.PopContext();

        public static void Debug(string message, params object[] fmtArgs) => instance.Debug(message, fmtArgs);
        public static void Info(string message, params object[] fmtArgs) => instance.Info(message, fmtArgs);
        public static void Warning(string message, params object[] fmtArgs) => instance.Warning(message, fmtArgs);
        public static void Error(string message, params object[] fmtArgs) => instance.Error(message, fmtArgs);
        public static void Error(Exception error) => instance.Error(error);
    }
}
