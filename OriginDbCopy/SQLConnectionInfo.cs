﻿using System.Collections.Specialized;

namespace OriginDbCopy
{
    public class SQLConnectionInfo
    {
        private SQLConnectionInfo(string server, string database, string user, string password, bool intSec)
        {
            IntegratedSecurity = string.IsNullOrEmpty(user);
            UserId = user;
            Password = password;
            Server = server;
            Database = database;
        }

        public SQLConnectionInfo(NameValueCollection appSettings)
        {
            Server = appSettings["SQLServer"];
            Database = appSettings["SQLDatabase"];
            UserId = appSettings["SQLUserId"];
            Password = appSettings["SQLPassword"];
            IntegratedSecurity = string.IsNullOrEmpty(UserId);
        }

        public SQLConnectionInfo(string server, string database) : this(server, database, null, null, true)
        {
        }

        public SQLConnectionInfo(string server, string database, string user, string password) : this(server, database, user, password, string.IsNullOrEmpty(user))
        {
        }

        public string Server { get; }

        public string Database { get; }

        public string UserId { get; }

        public string Password { get; }

        public bool IntegratedSecurity { get; }

        public bool TrustedConnection => IntegratedSecurity;

        public string ConnectionString => IntegratedSecurity ? string.Format("Server={0};Database={1};Integrated Security=True", Server, Database) : string.Format("Server={0};Database={1};User Id={2};Password={3}", Server, Database, UserId, Password);
    }
}
