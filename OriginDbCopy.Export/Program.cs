﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;
using Renci.SshNet;
using Renci.SshNet.Async;
using SimpleLogging;

namespace OriginDbCopy.Export
{
    sealed class ConnectionWrapper : IDisposable
    {
        private readonly ServerConnection serverConnection;

        public ConnectionWrapper(ServerConnection serverConnection)
        {
            this.serverConnection = serverConnection;
            serverConnection.Connect();
        }

        public void Dispose() => Dispose(true);

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                serverConnection.Disconnect();
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            if (!Directory.Exists(settings.LogPath))
            {
                Directory.CreateDirectory(settings.LogPath);
            }
            using (StaticLogger.Instance = new Logger(settings.LogLevel, new StreamWriter(File.OpenWrite(Path.Combine(settings.LogPath, DateTime.Now.ToString("yyyyMMdd") + ".txt")))))
            {
                using (var log = StaticLogger.PushContext(nameof(Main)))
                {
                    try
                    {
                        var p = new Program();
                        p.Run();
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex);
                    }
                }
            }
        }

        static readonly Settings settings = new Settings(ConfigurationManager.AppSettings);

        Program()
        {
        }

        void Run()
        {
            using (var log = StaticLogger.PushContext(nameof(Run)))
            {
                log.Info("Started at {0}", DateTime.Now);
                PreProcess();
                Process();
                PostProcess();
                log.Info("Finished at {0}", DateTime.Now);
            }
        }

        void CreateOutputPaths()
        {
            using (var log = StaticLogger.PushContext(nameof(CreateOutputPaths)))
            {
                var paths = new[]
                {
                    settings.SchemaPath, settings.FormatPath, settings.DataPath, settings.ZipPath, settings.LogPath
                };

                log.Info("Creating output paths");
                foreach (var path in paths)
                {
                    CreateOutputPath(path);
                }
            }
        }

        void CreateOutputPath(string path)
        {
            using (var log = StaticLogger.PushContext(nameof(CreateOutputPath)))
            {
                var pathInfo = new DirectoryInfo(path);
                if (!pathInfo.Exists)
                {
                    log.Debug("Creating directory {0}", path);
                    pathInfo.Create();
                }
            }
        }

        void ClearOutputPaths()
        {
            using (var log = StaticLogger.PushContext(nameof(ClearOutputPaths)))
            {
                var paths = new[]
                {
                    settings.SchemaPath, settings.FormatPath, settings.DataPath, settings.ZipPath
                };

                log.Info("Clearing output paths");
                foreach (var path in paths)
                {
                    ClearOutputPath(path);
                }
            }
        }

        void ClearOutputPath(string path)
        {
            using (var log = StaticLogger.PushContext(nameof(ClearOutputPath)))
            {
                DirectoryInfo pathInfo = new DirectoryInfo(path);

                log.Debug("Clearing directory {0}", path);
                var entries = pathInfo.GetFiles();
                foreach (var entry in entries)
                {
                    entry.Delete();
                }
            }
        }

        void PreProcess()
        {
            using (var log = StaticLogger.PushContext(nameof(PreProcess)))
            {
                log.Debug("PreProcess Started {0}", DateTime.Now);
                CreateOutputPaths();
                ClearOutputPaths();
                ExportDDLFiles();
                foreach (var table in settings.Tables)
                {
                    PreProcess(table);
                }
                log.Debug("PreProcess Finished {0}", DateTime.Now);
            }
        }

        void ExportDDLFiles()
        {
            using (var log = StaticLogger.PushContext(nameof(ExportDDLFiles)))
            {
                log.Info("Exporting DDL files");
                File.WriteAllText(Path.Combine(settings.SchemaPath, "schema.sql"), GenerateCreateTable(settings.Tables));
                File.WriteAllText(Path.Combine(settings.SchemaPath, "constraints.sql"), GenerateTableConstraints(settings.Tables));
            }
        }

        void PreProcess(Table table)
        {
            using (var log = StaticLogger.PushContext(nameof(PreProcess)))
            {
                ExportFormatFile(table);
            }
        }

        void ExportFormatFile(Table table)
        {
            using (var log = StaticLogger.PushContext(nameof(ExportFormatFile)))
            {
                var bcp = new BCP(settings.SQLConnectionInfo);
                log.Debug("Exporting format file for {0}", table.Name);
                bcp.CreateFormatFile(table, Path.Combine(settings.FormatPath, table.Name + ".xml"), settings);
            }
        }

        void PostProcess()
        {
            using (var log = StaticLogger.PushContext(nameof(PostProcess)))
            {
                log.Info("PostProcess Started {0}", DateTime.Now);
                CompressOutput();
                Upload();
                ClearOutputPaths();
                log.Info("PostProcess Finished {0}", DateTime.Now);
            }
        }

        void ZipDirectory(string directory, string zipfile)
        {
            using (var log = StaticLogger.PushContext(nameof(ZipDirectory)))
            {
                log.Debug("Compressing {0} -> {1}", directory, zipfile);
                var startedAt = DateTime.Now;
                ZipFile.CreateFromDirectory(directory, Path.Combine(settings.ZipPath, zipfile), CompressionLevel.Optimal, false);
                var finishedAt = DateTime.Now;
                log.Debug("Completed in {0}", finishedAt - startedAt);
            }
        }

        void CompressOutput()
        {
            using (var log = StaticLogger.PushContext(nameof(CompressOutput)))
            {
                log.Info("Compressing output");
                ZipDirectory(settings.SchemaPath, "schema.zip");
                ZipDirectory(settings.FormatPath, "format.zip");
                ZipDirectory(settings.DataPath, "data.zip");
            }
        }

        void Upload()
        {
            using (var log = StaticLogger.PushContext(nameof(Upload))) {
                var conInfo = settings.SFTPConnectionInfo;
                log.Debug("Connecting to SFTP");
                using (var sftp = new SftpClient(conInfo.Server, conInfo.Port, conInfo.UserId, conInfo.Password))
                {
                    sftp.Connect();
                    PreUpload(sftp).GetAwaiter().GetResult();

                    log.Debug("Uploading schema.zip");
                    using (var stream = File.OpenRead(Path.Combine(settings.ZipPath, "schema.zip")))
                    {
                        sftp.UploadAsync(stream, settings.SFTPSchemaPath + "/.schema.zip").GetAwaiter().GetResult();
                    }

                    log.Debug("Uploading format.zip");
                    using (var stream = File.OpenRead(Path.Combine(settings.ZipPath, "format.zip")))
                    {
                        sftp.UploadAsync(stream, settings.SFTPFormatPath + "/.format.zip").GetAwaiter().GetResult();
                    }

                    log.Debug("Uploading data.zip");
                    using (var stream = File.OpenRead(Path.Combine(settings.ZipPath, "data.zip")))
                    {
                        sftp.UploadAsync(stream, settings.SFTPDataPath + "/.data.zip").GetAwaiter().GetResult();
                    }

                    PostUpload(sftp).GetAwaiter().GetResult(); // Rename all ".<file>" to "<file>"
                }
            }
        }

        async Task PreUpload(SftpClient sftp)
        {
            using (var log = StaticLogger.PushContext(nameof(PreUpload)))
            {
                await ClearSFTPPaths(sftp);
            }
        }

        async Task ClearSFTPPaths(SftpClient sftp)
        {
            using (var log = StaticLogger.PushContext(nameof(ClearSFTPPaths)))
            {
                // Delete all files in SFTP*Path
                var sftpPaths = new[]
                {
                    settings.SFTPSchemaPath, settings.SFTPFormatPath, settings.SFTPDataPath
                };
                log.Info("Clearing remote paths");
                foreach (var sftpPath in sftpPaths)
                {
                    await ClearSFTPPath(sftp, sftpPath);
                }
            }
        }

        async Task ClearSFTPPath(SftpClient sftp, string sftpPath)
        {
            using (var log = StaticLogger.PushContext(nameof(ClearSFTPPath)))
            {
                log.Debug("Clearing remote directory {0}", sftpPath);
                foreach (var sftpFile in await sftp.ListDirectoryAsync(sftpPath))
                {
                    if (sftpFile.IsRegularFile)
                    {
                        sftpFile.Delete();
                    }
                }
            }
        }

        async Task PostUpload(SftpClient sftp)
        {
            using (var log = StaticLogger.PushContext(nameof(PostUpload)))
            {
                // Rename all ".<file>" to "<file"> in SFTP{Schema,Format,Data}Path
                string[] paths = new[]
                {
                    settings.SFTPSchemaPath, settings.SFTPFormatPath, settings.SFTPDataPath
                };

                foreach (var path in paths)
                {
                    await PostUpload(sftp, path);
                }
            }
        }

        async Task PostUpload(SftpClient sftp, string path)
        {
            using (var log = StaticLogger.PushContext(nameof(PostUpload)))
            {
                foreach (var file in await sftp.ListDirectoryAsync(path))
                {
                    if (file.IsRegularFile && file.Name.StartsWith("."))
                    {
                        log.Debug("Renaming remote file {0} to {1}", file.Name, file.Name.Substring(1));
                        file.MoveTo(path + "\\" + file.Name.Substring(1));
                    }
                }
            }
        }

        void Process()
        {
            using (var log = StaticLogger.PushContext(nameof(Process)))
            {
                log.Info("Process Started {0}", DateTime.Now);
                foreach (var table in settings.Tables)
                {
                    Process(table);
                }
                log.Info("Process Finsihed {0}", DateTime.Now);
            }
        }

        private void Process(Table table)
        {
            using (var log = StaticLogger.PushContext(nameof(Process)))
            {
                ExportDataFile(table);
            }
        }

        private void ExportDataFile(Table table)
        {
            using (var log = StaticLogger.PushContext(nameof(ExportDataFile)))
            {
                // Export data to settings.DataPath\<table>.bcp
                var bcp = new BCP(settings.SQLConnectionInfo);
                log.Debug("Exporting data for {0}", table.Name);
                bcp.ExportDataFile(table, Path.Combine(settings.DataPath, table.Name + ".bcp"), Path.Combine(settings.FormatPath, table.Name + ".xml"), settings);
            }
        }

        string GenerateCreateTable(Table table) =>
            string.Join("\n", GenerateDDL(table, ScriptOption.Default).Cast<string>());

        string GenerateTableConstraints(Table table) =>
            string.Join("\n", GenerateDDL(table, ScriptOption.DriAll).Cast<string>());

        string GenerateCreateTable(IEnumerable<Table> tables) =>
            string.Join("\n", GenerateDDL(tables, ScriptOption.Default).Cast<string>());

        string GenerateTableConstraints(IEnumerable<Table> tables) =>
            string.Join("\n", GenerateDDL(tables, ScriptOption.DriAll).Cast<string>());

        StringCollection GenerateDDL(Table table, ScriptingOptions options)
        {
            using (var log = StaticLogger.PushContext(nameof(GenerateDDL)))
            {
                var conInfo = settings.SQLConnectionInfo;
                var server = new Server(conInfo.Server);
                if (!(server.ConnectionContext.LoginSecure = conInfo.IntegratedSecurity))
                {
                    var ctx = server.ConnectionContext;
                    ctx.Login = conInfo.UserId;
                    ctx.Password = conInfo.Password;
                }
                log.Debug("Connecting to Sql Server");
                using (new ConnectionWrapper(server.ConnectionContext))
                {
                    var db = server.Databases[conInfo.Database];
                    Microsoft.SqlServer.Management.Smo.Table dbTable = null;
                    if (!string.IsNullOrEmpty(table.Schema))
                    {
                        dbTable = db.Tables[table.Name, table.Schema];
                    }
                    else
                    {
                        dbTable = db.Tables[table.Name];
                    }
                    log.Info("Generating script for table {0}", table.Name);
                    return dbTable.Script(options);
                }
            }
        }

        StringCollection GenerateDDL(IEnumerable<Table> tables, ScriptingOptions options)
        {
            using (var log = StaticLogger.PushContext(nameof(GenerateDDL)))
            {
                var conInfo = settings.SQLConnectionInfo;
                var server = new Server(conInfo.Server);
                if (!(server.ConnectionContext.LoginSecure = conInfo.IntegratedSecurity))
                {
                    var ctx = server.ConnectionContext;
                    ctx.Login = conInfo.UserId;
                    ctx.Password = conInfo.Password;
                }
                log.Debug("Connecting to Sql Server");
                using (new ConnectionWrapper(server.ConnectionContext))
                {
                    var db = server.Databases[conInfo.Database];
                    if (db == null)
                    {
                        log.Error("Unable to find database name {0}", conInfo.Database);
                    }
                    var tableUrns = tables.Select(t => db.Tables[t.Name].Urn).ToArray();
                    var scripter = new Scripter(server)
                    {
                        Options = options
                    };
                    log.Debug("Generating script for tables {0}", string.Join(", ", tables.Select(table => table.Name)));
                    return scripter.Script(tableUrns);
                }
            }
        }
    }
}
